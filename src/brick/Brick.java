package src.brick;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;

import acm.graphics.GLabel;
import acm.graphics.GObject;
import acm.graphics.GOval;
import acm.graphics.GPoint;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import acm.util.RandomGenerator;

public class Brick extends GraphicsProgram {

	/** Width and height of application window in pixels **/
	public static final int APPLICATION_WIDTH = 400;
	public static final int APPLICATION_HEIGHT = 650;

	/** Dimensions of game board **/
	private static final int WIDTH = APPLICATION_WIDTH;
	private static final int BOTTOM_OFFSET = 50;
	private static final int HEIGHT = APPLICATION_HEIGHT - BOTTOM_OFFSET;

	/** Dimensions of the paddle **/
	private static final int PADDLE_WIDTH = 60;
	private static final int PADDLE_HEIGHT = 10;

	/** Offset of the paddle up from the bottom **/
	private static final int PADDLE_Y_OFFSET = 30;

	/** Number of bricks per row **/
	private static final int NBRICKS_PER_ROW = 10;

	/** Number of rows of bricks **/
	private static final int NBRICK_ROWS = 10;

	/** Separation between bricks **/
	private static final int BRICK_SEP = 4;

	/** Width of a brick **/
	private static final int BRICK_WIDTH = (WIDTH - (NBRICKS_PER_ROW + 1)
			* BRICK_SEP)
			/ NBRICKS_PER_ROW;

	/** Height of a brick **/
	private static final int BRICK_HEIGHT = 8;

	/** Radius of the ball in pixels **/
	private static final int BALL_RADIUS = 5;

	/** Offset of the top brick row from the top **/
	private static final int BRICK_Y_OFFSET = 50;

	/** Number of lives **/
	private static final int NLIVES = 3;

	/** Game speed **/
	private static final int DELAY = 25;

	/** Velocity **/
	private static final int BALL_VELOCITY_Y = 3;
	private static final int BALL_VELOCITY_X_MAX = 6;
	private static final int BALL_VELOCITY_X_MIN = 3;

	/** Variance on the X velocity for Derp Mode **/
	private static final int BALL_X_VAR = 10;

	/** Color Scheme **/
	private Color ballColor;
	private Color backgroundColor;
	private Color paddleColor;
	private Color[] brickColors;

	/** Game elements **/
	private GRect paddle;
	private GOval ball;
	private GOval ball1;
	private GOval ball2;
	private GRect[][] bricks;
	private GLabel brickLabel;
	private double yVelocity;
	private double xVelocity;
	private int lives;
	private int totalBricks;
	private int ticks;
	private boolean isPaddleHit;
	private boolean isPaused;
	private boolean derpMode;

	/** Random Number Generator **/
	private RandomGenerator rgen = RandomGenerator.getInstance();

	/** Mouse motion tracking **/
	public void mouseMoved(MouseEvent e) {
		if (!isPaused) {
			double x = e.getX();
			if (x > WIDTH - PADDLE_WIDTH / 2) {
				x = WIDTH - PADDLE_WIDTH / 2;
			}
			else if (x < PADDLE_WIDTH / 2) {
				x = PADDLE_WIDTH / 2;
			}

			paddle.setLocation(x - PADDLE_WIDTH / 2, HEIGHT - PADDLE_Y_OFFSET);
		}
	}

	/** Mouse click tracking **/
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			isPaused ^= true;
		}
		if (e.getButton() == MouseEvent.BUTTON2 && e.isShiftDown()
				&& e.isControlDown()) {
			derpMode ^= true;
			if (derpMode) {
				ball.setColor(Color.RED);
				ball.setFillColor(Color.RED);
				ball1.setColor(Color.RED);
				ball1.setFillColor(Color.RED);
				ball2.setColor(Color.RED);
				ball2.setFillColor(Color.RED);
				brickLabel.setColor(Color.RED);
			}
			else {
				ball.setColor(ballColor);
				ball.setFillColor(ballColor);
				ball1.setColor(ballColor);
				ball1.setFillColor(ballColor);
				ball2.setColor(ballColor);
				ball2.setFillColor(ballColor);
				brickLabel.setColor(Color.BLACK);
			}
		}
	}

	/** Change the Y velocity on collision **/
	private void changeYVelocity() {
		yVelocity *= -1;
	}

	/** Change the X velocity of collision **/
	private void changeXVelocity() {
		xVelocity *= -1;
	}

	/** Change the X velocity with respect the paddle **/
	private void changeXVelocity(double difference) {
		difference = difference < 0 ? -difference : difference;
		if (difference > PADDLE_WIDTH / 2) {
			xVelocity = xVelocity > 0
					? BALL_VELOCITY_X_MAX : -BALL_VELOCITY_X_MAX;
		}
		else {
			xVelocity = xVelocity > 0 ? BALL_VELOCITY_X_MIN
					+ (BALL_VELOCITY_X_MAX - BALL_VELOCITY_X_MIN) * 2
					* difference / PADDLE_WIDTH : -BALL_VELOCITY_X_MIN
					- (BALL_VELOCITY_X_MAX - BALL_VELOCITY_X_MIN) * 2
					* difference / PADDLE_WIDTH;
		}
	}

	/** Resets the ball **/
	public void resetBall() {
		ball.setLocation(WIDTH / 2 - BALL_RADIUS, HEIGHT / 2 - BALL_RADIUS);
		yVelocity = yVelocity < 0 ? -yVelocity : yVelocity;
		xVelocity = rgen.nextDouble(BALL_VELOCITY_X_MIN, BALL_VELOCITY_X_MAX);
		xVelocity = rgen.nextBoolean() ? xVelocity : -xVelocity;
	}

	/** Hides the paddle and the ball **/
	public void hideUI() {
		paddle.setVisible(false);
		ball.setVisible(false);
	}

	/** Shows the paddle and the ball **/
	public void showUI() {
		paddle.setVisible(true);
		ball.setVisible(true);
	}

	/** Updates the brick count for the label **/
	public void updateLabel() {
		brickLabel.setLabel("Bricks Left: " + totalBricks);
	}

	public void updateLives() {
		if (lives == 2) {
			remove(ball2);
		}
		else if (lives == 1) {
			remove(ball1);
		}
	}

	/** Start the game **/
	public void run() {
		setup();
		while (true) {
			resetAll();
			showInfo();
			intermission();
			while (lives > 0 && totalBricks != 0) {
				if (isPaused) {
					hideUI();
					intermission();
					showUI();
				}
				moveBall();
				pause(ticks);
				if (checkCollision(ball.getX(), ball.getY())) {
					resetBall();
					lives--;
					updateLives();
					intermission();
				}
			}
			showGameStatus();
			removeAll();
		}
	}

	/** Setup the board **/
	public void setup() {
		paddleColor = new Color(0, 0, 0);
		ballColor = new Color(144, 144, 144);
		backgroundColor = new Color(250, 250, 250);
		brickColors = new Color[] { Color.RED, Color.ORANGE,
				new Color(235, 235, 0), Color.GREEN, Color.CYAN };
		setSize(APPLICATION_WIDTH, APPLICATION_HEIGHT);
		setMinimumSize(getSize());
		setMaximumSize(getSize());
		setTitle("Brick");
		setBackground(backgroundColor);

		bricks = new GRect[NBRICK_ROWS][NBRICKS_PER_ROW];
		double offsetY = BRICK_Y_OFFSET;
		for (int i = 0; i < NBRICK_ROWS; i++) {
			double offsetX = BRICK_SEP;
			for (int j = 0; j < NBRICKS_PER_ROW; j++) {
				Color c = brickColors[(Integer) ((i / 2) < brickColors.length
						? (i / 2) : (brickColors[brickColors.length - 1]))];
				bricks[i][j] = new GRect(offsetX, offsetY, BRICK_WIDTH,
						BRICK_HEIGHT);
				bricks[i][j].setVisible(true);
				bricks[i][j].setFilled(true);
				bricks[i][j].setColor(c);
				bricks[i][j].setFillColor(c);
				offsetX += (BRICK_WIDTH + BRICK_SEP);
			}
			offsetY += (BRICK_HEIGHT + BRICK_SEP);
		}

		paddle = new GRect((WIDTH - PADDLE_WIDTH) / 2,
				HEIGHT - PADDLE_Y_OFFSET, PADDLE_WIDTH, PADDLE_HEIGHT);
		paddle.setFilled(true);
		paddle.setColor(paddleColor);
		paddle.setFillColor(paddleColor);

		ball = new GOval(WIDTH / 2 - BALL_RADIUS, HEIGHT / 2 - BALL_RADIUS,
				2 * BALL_RADIUS, 2 * BALL_RADIUS);
		ball.setFilled(true);

		ball1 = new GOval(WIDTH - 5 * BALL_RADIUS, APPLICATION_HEIGHT
				- (APPLICATION_HEIGHT - HEIGHT + BALL_RADIUS) / 2,
				2 * BALL_RADIUS, 2 * BALL_RADIUS);
		ball1.setFilled(true);
		ball1.setColor(ballColor);
		ball1.setFillColor(ballColor);

		ball2 = new GOval(WIDTH - 10 * BALL_RADIUS, APPLICATION_HEIGHT
				- (APPLICATION_HEIGHT - HEIGHT + BALL_RADIUS) / 2,
				2 * BALL_RADIUS, 2 * BALL_RADIUS);
		ball2.setFilled(true);
		ball2.setColor(ballColor);
		ball2.setFillColor(ballColor);

		brickLabel = new GLabel("Bricks Left: 100");
		brickLabel.setFont(new Font("Arial", Font.BOLD, 14));
		brickLabel.setLocation((WIDTH - brickLabel.getWidth()) / 2, APPLICATION_HEIGHT
				- (APPLICATION_HEIGHT - HEIGHT - brickLabel.getHeight()) / 2);

		addMouseListeners();
	}

	/** Resets all objects **/
	public void resetAll() {
		paddle.setLocation((WIDTH - PADDLE_WIDTH) / 2, HEIGHT - PADDLE_Y_OFFSET);
		add(paddle);

		ball.setLocation(WIDTH / 2 - BALL_RADIUS, HEIGHT / 2 - BALL_RADIUS);
		ball.setColor(ballColor);
		ball.setFillColor(ballColor);
		add(ball);
		add(ball1);
		add(ball2);

		for (int i = 0; i < NBRICK_ROWS; i++) {
			for (int j = 0; j < NBRICKS_PER_ROW; j++) {
				add(bricks[i][j]);
			}
		}

		totalBricks = NBRICK_ROWS * NBRICKS_PER_ROW;
		updateLabel();
		add(brickLabel);

		yVelocity = BALL_VELOCITY_Y;
		xVelocity = rgen.nextDouble(BALL_VELOCITY_X_MIN, BALL_VELOCITY_X_MAX);
		xVelocity = rgen.nextBoolean() ? xVelocity : -xVelocity;
		lives = NLIVES;
		ticks = DELAY;
		isPaddleHit = false;
		isPaused = false;
		derpMode = false;
	}

	/** Display game controls **/
	public void showInfo() {
		GLabel titleLabel = new GLabel("--- WELCOME TO BRICK LAND ---");
		titleLabel.setLocation((WIDTH - titleLabel.getWidth()) / 2, HEIGHT / 2
				- 3 * titleLabel.getHeight());
		titleLabel.setColor(Color.WHITE);

		GLabel columnLabel = new GLabel("Control              Action");
		columnLabel.setLocation((WIDTH - columnLabel.getWidth()) / 2, titleLabel.getY()
				+ titleLabel.getHeight() * 1.25);
		columnLabel.setColor(Color.WHITE);

		GLabel borderLabel = new GLabel("---------------------------------");
		borderLabel.setLocation((WIDTH - borderLabel.getWidth()) / 2, columnLabel.getY()
				+ columnLabel.getHeight());
		borderLabel.setColor(Color.WHITE);

		GLabel controlLabel1 = new GLabel("Click                 Pause");
		controlLabel1.setLocation((WIDTH - controlLabel1.getWidth()) / 2, borderLabel.getY()
				+ borderLabel.getHeight());
		controlLabel1.setColor(Color.WHITE);

		GLabel controlLabel2 = new GLabel("Click             Unpause");
		controlLabel2.setLocation((WIDTH - controlLabel2.getWidth()) / 2, controlLabel1.getY()
				+ controlLabel1.getHeight());
		controlLabel2.setColor(Color.WHITE);

		GLabel controlLabel3 = new GLabel("Mouse                Move");
		controlLabel3.setLocation((WIDTH - controlLabel3.getWidth()) / 2, controlLabel2.getY()
				+ controlLabel2.getHeight());
		controlLabel3.setColor(Color.WHITE);

		GLabel hintLabel = new GLabel("Click to Continue");
		hintLabel.setLocation((WIDTH - hintLabel.getWidth()) / 2, controlLabel3.getY()
				+ controlLabel3.getHeight() * 1.25);
		hintLabel.setColor(Color.MAGENTA);

		GRect backPanel = new GRect(titleLabel.getX() - titleLabel.getWidth()
				* 0.125, titleLabel.getY() - titleLabel.getHeight() * 1.5,
				titleLabel.getWidth() * 1.25,
				(hintLabel.getY() - titleLabel.getY())
						+ (hintLabel.getHeight() + titleLabel.getHeight()));
		backPanel.setFilled(true);
		backPanel.setFillColor(Color.DARK_GRAY);
		backPanel.setColor(Color.DARK_GRAY);

		add(backPanel);
		add(titleLabel);
		add(columnLabel);
		add(borderLabel);
		add(controlLabel1);
		add(controlLabel2);
		add(controlLabel3);
		add(hintLabel);

		isPaused = true;
		while (isPaused) {
			pause(500);
		}
		remove(backPanel);
		remove(titleLabel);
		remove(columnLabel);
		remove(borderLabel);
		remove(controlLabel1);
		remove(controlLabel2);
		remove(controlLabel3);
		remove(hintLabel);
	}

	/** Display status **/
	public void intermission() {
		GLabel livesLabel = new GLabel("You have " + lives
				+ (lives == 1 ? " ball" : " balls") + " remaining.");
		livesLabel.setLocation((WIDTH - livesLabel.getWidth()) / 2, (HEIGHT - livesLabel.getHeight()) / 2);
		livesLabel.setColor(Color.WHITE);

		GLabel hintLabel = new GLabel("Click to Continue");
		hintLabel.setLocation((WIDTH - hintLabel.getWidth()) / 2, (HEIGHT - hintLabel.getHeight())
				/ 2 + livesLabel.getHeight());
		hintLabel.setColor(Color.MAGENTA);

		GRect livesPanel = new GRect(livesLabel.getX() - livesLabel.getWidth()
				* 0.125, livesLabel.getY() - livesLabel.getHeight(),
				livesLabel.getWidth() * 1.25, livesLabel.getHeight() * 1.25
						+ hintLabel.getHeight());
		livesPanel.setFilled(true);
		livesPanel.setFillColor(Color.DARK_GRAY);
		livesPanel.setColor(Color.DARK_GRAY);

		add(livesPanel);
		add(livesLabel);
		add(hintLabel);

		isPaused = true;
		while (isPaused) {
			pause(500);
		}
		remove(livesLabel);
		remove(hintLabel);
		remove(livesPanel);
	}

	/** Moves the ball **/
	public void moveBall() {
		if (!derpMode) {
			ball.move(xVelocity, yVelocity);
		}
		else {
			if (rgen.nextBoolean(0.5)) {
				ball.move(xVelocity + BALL_X_VAR, yVelocity);
			}
			else {
				ball.move(xVelocity - BALL_X_VAR, yVelocity);
			}

			if (ball.getX() < 0) {
				ball.setLocation(0, ball.getY());
			}
			else if (ball.getX() + 2 * BALL_RADIUS > WIDTH) {
				ball.setLocation(WIDTH - 2 * BALL_RADIUS, ball.getY());
			}
		}
	}

	/** Check for collision. Returns true if player loses the ball **/
	public boolean checkCollision(double ballX, double ballY) {
		GPoint ballTop = new GPoint(ballX + BALL_RADIUS + 1, ballY);
		GPoint ballLeft = new GPoint(ballX, ballY + BALL_RADIUS + 1);
		GPoint ballRight = new GPoint(ballX + 2 * BALL_RADIUS + 1, ballY
				+ BALL_RADIUS + 1);
		GPoint ballBottom = new GPoint(ballX + BALL_RADIUS + 1, ballY + 2
				* BALL_RADIUS + 1);

		if (ballLeft.getX() <= 0 || ballRight.getX() >= WIDTH) {
			changeXVelocity();
			isPaddleHit = false;
		}
		if (ballTop.getY() <= 0) {
			changeYVelocity();
			isPaddleHit = false;
			return false;
		}
		else if (ballBottom.getY() >= HEIGHT) {
			isPaddleHit = false;
			return true;
		}

		GObject collider = getCollidingObject(ballTop, ballLeft, ballRight, ballBottom);

		if (collider != null) {
			if (collider != paddle) {
				isPaddleHit = false;
				if (--totalBricks % 5 == 0) {
					ticks--;
				}
				updateLabel();
				remove(collider);
			}
			else {
				yVelocity = yVelocity < 0 ? yVelocity : -yVelocity;
				if (!isPaddleHit) {
					isPaddleHit = true;
					changeXVelocity(ballBottom.getX()
							- (paddle.getX() + PADDLE_WIDTH / 2));
				}
			}
		}

		return false;
	}

	/** Returns the object of collision or null if there are no collisions **/
	public GObject getCollidingObject(GPoint ballTop, GPoint ballLeft,
			GPoint ballRight, GPoint ballBottom) {
		GObject collider = null;
		if ((collider = getElementAt(ballTop)) != null) {
			changeYVelocity();
		}
		else if ((collider = getElementAt(ballBottom)) != null) {
			changeYVelocity();
		}
		else if ((collider = getElementAt(ballLeft)) != null) {
			changeXVelocity();
		}
		else if ((collider = getElementAt(ballRight)) != null) {
			changeXVelocity();
		}

		return collider;
	}

	/** Displays a winning or losing message. **/
	public void showGameStatus() {
		GLabel messageLabel;
		if (lives <= 0) {
			messageLabel = new GLabel("You lose THE GAME of Brick. Try harder.");
			messageLabel.setLocation((WIDTH - messageLabel.getWidth()) / 2, (HEIGHT - messageLabel.getHeight()) / 2);
			messageLabel.setColor(Color.WHITE);
		}
		else {
			messageLabel = new GLabel("You win THE GAME of Brick. Savour it.");
			messageLabel.setLocation((WIDTH - messageLabel.getWidth()) / 2, (HEIGHT - messageLabel.getHeight()) / 2);
			messageLabel.setColor(Color.WHITE);
		}

		GLabel directionLabel = new GLabel("Click to Start a New Game");
		directionLabel.setLocation((WIDTH - directionLabel.getWidth()) / 2, (HEIGHT - directionLabel.getHeight())
				/ 2 + messageLabel.getHeight());
		directionLabel.setColor(Color.MAGENTA);

		GRect bgPanel = new GRect(messageLabel.getX() - messageLabel.getWidth()
				* 0.125, messageLabel.getY() - messageLabel.getHeight(),
				messageLabel.getWidth() * 1.25, messageLabel.getHeight() * 1.25
						+ directionLabel.getHeight());
		bgPanel.setFilled(true);
		bgPanel.setFillColor(Color.DARK_GRAY);
		bgPanel.setColor(Color.DARK_GRAY);

		add(bgPanel);
		add(messageLabel);
		add(directionLabel);
		isPaused = true;
		while (isPaused) {
			pause(500);
		}
		remove(messageLabel);
		remove(directionLabel);
		remove(bgPanel);
	}
}
